# CMake generated Testfile for 
# Source directory: /home/fofo/ROS_projects/ardrone_controller_ws/src
# Build directory: /home/fofo/ROS_projects/ardrone_controller_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(ardrone_controller_pkg)
